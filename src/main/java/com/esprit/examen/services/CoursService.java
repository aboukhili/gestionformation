package com.esprit.examen.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.examen.entities.Cours;
import com.esprit.examen.repositories.CoursRepository;

@Service
public class CoursService implements ICoursService {

	private static final Logger logger = LogManager.getLogger(CoursService.class);
	private static final String ENTRDEB = "Cours";


	@Autowired
	CoursRepository coursRepository;
	@Override
	public Long addCours(Cours cours) {
		logger.info("Add of Cours : ");
		logger.debug(ENTRDEB,cours );
		try {
		cours=coursRepository.save(cours);
		logger.info("Add cours success.");
		return cours.getId();
	}catch (Exception e) {
		// TODO: handle exception
		logger.error("Add cours failed",e);
	}
		return null;
	}
	@Override
	public void supprimerCours(Long coursId) {
		logger.info("Delete of Cours : ");
		logger.debug(ENTRDEB,coursId );
		try {
		coursRepository.deleteById(coursId);
		logger.info("Delete cours success.");
		}catch (Exception e) {
			// TODO: handle exception
			logger.error("Delete cours failed",e);
		}
	}

	@Override
	public List<Cours> getCours() {		
		List<Cours> cours =   coursRepository.findAll();
		return cours;
	}
	
	@Override
	public void affecterCoursASession(Long coursId, Long sessionId)
	{
		/*todo*/
        
	}

}
