package com.esprit.examen.services;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.TypeCours;
import com.esprit.examen.exception.NotFoundException;
import com.esprit.examen.repositories.FormateurRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Service
public class FormateurService implements IFormateurService {
	private static final Logger logger = LogManager.getLogger(FormateurService.class);
	private static final String ENTRDEB = "Formateur";
	private static final Logger LOG = LogManager.getLogger(FormateurService.class);

	@Autowired
	FormateurRepository formateurRepository;

	@Override
	public Long addFormateur(Formateur formateur) {
		logger.info("In ajouterFormateur : ");
		logger.debug(ENTRDEB, formateur);
		try {
			formateurRepository.save(formateur);
			logger.info("Out ajouterFormateur() without errors.");
			return formateur.getId();
		} catch (Exception e) {
			logger.error("Erreur dans ajouterFormateur() : ", e);
			return 0l;
		}

	}

	@Override
	public Long nombreFormateursImpliquesDansUnCours(TypeCours typeCours) {
		return formateurRepository.nombreFormateursImpliquesDansUnCours(typeCours);

	}

	@Override
	public List<Formateur> listFormateurs() {
		LOG.info("listFormateurs() ");
		LOG.debug("Accéss à la base de données");
		LOG.info("Out listFormateurs()");
		return formateurRepository.findAll();
	}

	@Override
	public double noterFormateur(double note, long id) throws NotFoundException {
		LOG.info(MessageFormat.format("NOTE", note));
		LOG.info(MessageFormat.format("Formateur id", id));
		double moyenne = 0;
		if (note < 0 || note > 20) {
			return note;
		}

		if (formateurRepository.findById(id).isPresent()) {
			Formateur formateur = formateurRepository.findById(id).orElseThrow(NotFoundException::new);
			LOG.info(MessageFormat.format("Formateur id", formateur.getId()));
			moyenne = formateur.getNote();
			int nbr = formateur.getNumber() + 1;
			moyenne = moyenne + note / nbr;
			formateur.setNote(moyenne);
			formateur.setNumber(nbr);
			formateurRepository.save(formateur);
			LOG.info(MessageFormat.format("Formateur id", formateur));

		}

		return moyenne;

	}

	@Override
	public Formateur modifierEmailFormateur(String nom, Formateur formateur) {
		LOG.info(MessageFormat.format("Nom", nom));
		LOG.info(MessageFormat.format("Formateur", formateur));
		Formateur form = formateurRepository.getFormateurByName(nom);
		if (form != null) {
			form.setEmail(formateur.getEmail());
			return formateurRepository.save(form);
		} else

			return null;
	}

	@Override
	public void deleteFormateurById(long formateurId) {
        LOG.info("In delete formateur by id : ");
		LOG.debug("Accéss à la base de données");
		formateurRepository.deleteById(formateurId);

	}

}
