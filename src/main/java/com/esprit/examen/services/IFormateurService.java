package com.esprit.examen.services;

import java.util.List;
import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.TypeCours;
import com.esprit.examen.exception.NotFoundException;

public interface IFormateurService {
	Long addFormateur(Formateur formateur);


	
	
	Long nombreFormateursImpliquesDansUnCours(TypeCours typeCours);
		
	List<Formateur> listFormateurs();

	double noterFormateur(double note, long id) throws NotFoundException;

	void deleteFormateurById(long formateurId);

	Formateur modifierEmailFormateur(String nom,Formateur formateur);

	
}
