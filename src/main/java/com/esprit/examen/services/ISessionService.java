package com.esprit.examen.services;


import java.util.List;


import com.esprit.examen.entities.Session;

public interface ISessionService {
	

	Long modifierSession(Session session);
	void affecterCoursASession(Long coursId, Long sessionId) ;
	void supprimerSession(Long sessionId);
	List<Session> listSession();
}
