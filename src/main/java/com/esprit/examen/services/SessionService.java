package com.esprit.examen.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.esprit.examen.entities.Cours;
import com.esprit.examen.entities.Session;
import com.esprit.examen.repositories.CoursRepository;
import com.esprit.examen.repositories.SessionRepository;
import java.util.Collections;

@Service
public class SessionService implements ISessionService{
	private static final Logger logger = LogManager.getLogger(SessionService.class);
	private static final String ENTRDEB = "Session";
	CoursRepository coursRepository;
	@Autowired
	SessionRepository sessionRepository;
	@Override
	public Long modifierSession(Session session) {
		logger.info("In modifierSession : ");
		logger.debug(ENTRDEB, session);
		try {
		sessionRepository.save(session);
		logger.info("Out modifierSession() without errors.");
		return session.getId();
		} catch (Exception e) {
			logger.error("Erreur dans modifierSession() : ", e);
			return 0l;
		}
	}

	@Override
	public void supprimerSession(Long sessionId) {
		logger.info("In supprimerSession : ");
		logger.debug(ENTRDEB, sessionId);
		try {
		sessionRepository.deleteById(sessionId);
		logger.info("Out supprimerSession() without errors.");
		} catch (Exception e) {
			logger.error("Erreur dans supprimerSession() : ", e);
			
		}
	}
	@Override
public List<Session> listSession() {
		logger.info("In listSession : ");
		
	try {
		logger.info("Out listSession() without errors.");
		return sessionRepository.findAll();
		
	} catch (Exception e) {
		logger.error("Erreur dans listSession() : ", e);
		return Collections.emptyList();
	}
	}


	@Override
	public void affecterCoursASession(Long coursId, Long sessionId) {
		Optional<Cours> optcours = coursRepository.findById(coursId);
		Optional<Session> optsession = sessionRepository.findById(sessionId);
		if(optcours.isPresent() && optsession.isPresent()) {
		Cours cour = optcours.get();
		Session session = optsession.get();
		Set<Session> sessionSet = cour.getSessions();
		sessionSet.add(session);
		cour.setSessions(sessionSet);
		coursRepository.save(cour);
		Set<Cours> courSet = session.getCours();
		courSet.add(cour);
		sessionRepository.save(session);
		
	}}





}
