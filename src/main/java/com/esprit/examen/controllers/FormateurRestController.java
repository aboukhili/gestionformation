package com.esprit.examen.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.TypeCours;
import com.esprit.examen.exception.NotFoundException;
import com.esprit.examen.services.IFormateurService;

@RestController
public class FormateurRestController {

	@Autowired
	IFormateurService formateurService;
	
	@PostMapping("/ajouterFormateur")
	@ResponseBody
	public Formateur ajouterFormateur(@RequestBody Formateur formateur) {
		formateurService.addFormateur(formateur);
		return formateur;
	}

	@PutMapping("/modifierFormateur/{nom}")
	@ResponseBody
	public Formateur modifierFormateur( @PathVariable("nom") String nom,@RequestBody Formateur formateur) {
		formateurService.modifierEmailFormateur(nom, formateur);
		return formateur;
	}

	
	
	@GetMapping("/nombreFormateursImpliquesDansUnCours/{typeCours}")
	@ResponseBody
	public Long nombreFormateursImpliquesDansUnCours(@PathVariable("typeCours") TypeCours typeCours) {
		
		return formateurService.nombreFormateursImpliquesDansUnCours(typeCours);
	}
		@DeleteMapping("/deleteFormateurById/{idFormateur}")
		@ResponseBody
		public void deleteFormateurById(@PathVariable("idFormateur") long formateurId) {
			formateurService.deleteFormateurById(formateurId);
		}

	
	@PostMapping("/noterFormateur/{id}/{note}")
	@ResponseBody
	public double noterFormateur( @PathVariable(value="note") double note, @PathVariable(value="id") long id) throws NotFoundException {
	return	formateurService.noterFormateur(note , id);
	
	}
}
