package com.esprit.examen;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.annotation.Id;
import org.springframework.test.context.junit4.SpringRunner;



import com.esprit.examen.entities.Contrat;
import com.esprit.examen.entities.Cours;
import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.Poste;
import com.esprit.examen.entities.Session;
import com.esprit.examen.repositories.FormateurRepository;
import com.esprit.examen.repositories.SessionRepository;
import com.esprit.examen.services.FormateurService;
import com.esprit.examen.services.SessionService;

import junit.framework.*;




@SpringBootTest
@RunWith(SpringRunner.class)



public class SessionTest {
@Autowired
SessionRepository sessionRepository;
@Autowired
SessionService sessionService;
private Session session;
private Cours cours;
@Before
public void setUp()
{
this.session = new Session();
this.session.setId(1L);
this.session.setDuree(2L);
this.session.setDescription("DevOps");
this.cours=new Cours();
this.cours.setId(1L);

}



//@Test
//public void deleteSessionByIdTest() {
//
//	sessionService.supprimerSession(session.getId());
//	assertThat(session.getId()).isNotNegative();
//}

@Test
public void modifierSessionTest() {
	Long idSession = sessionService.modifierSession(session);
	assertEquals(session.getId(), idSession);

}

@Test
public void ListSessionTest() {
	List<Session> listS= sessionService.listSession();
	assertNotNull(listS);
}







}

