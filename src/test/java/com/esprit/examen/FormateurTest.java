package com.esprit.examen;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.text.ParseException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.annotation.Id;
import org.springframework.test.context.junit4.SpringRunner;

import com.esprit.examen.entities.Contrat;
import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.Poste;
import com.esprit.examen.exception.NotFoundException;
import com.esprit.examen.repositories.FormateurRepository;
import com.esprit.examen.services.FormateurService;

import junit.framework.*;

@SpringBootTest
@RunWith(SpringRunner.class)

public class FormateurTest {
	@Autowired
	FormateurRepository formateurRepository;
	@Autowired
	FormateurService formateurService;
	private Formateur formateur;

	@Before
	public void setUp() {
		this.formateur = new Formateur();
		this.formateur.setEmail("aboukhili@vermeg.com");
		this.formateur.setNom("Amal");
		this.formateur.setNote(10);
		this.formateur.setNumber(3);
		this.formateur.setContrat(Contrat.CIVP);
		this.formateur.setPoste(Poste.INGENIEUR);

	}

	@Test
	public void tests() throws ParseException, NotFoundException {
		ajouterFormateurTest();
		modifierEmailFormateurTest();
	    noterFormateurTest();
		listFormateursTest();
		deleteFormateurByIdTest();

	}

	public void ajouterFormateurTest() {
		Long idFormateur = formateurService.addFormateur(formateur);
		assertEquals(formateur.getId(), idFormateur);
	}

	public void modifierEmailFormateurTest() {
		String email = formateur.getEmail();
		formateur.setEmail("aboukhili222@vermeg.com");
		formateur = formateurService.modifierEmailFormateur(formateur.getNom(), formateur);
		assertNotEquals(formateur.getEmail(), email);
	}

	public void deleteFormateurByIdTest() {

		formateurService.deleteFormateurById(formateur.getId());
		assertThat(formateur.getId()).isNotNegative();
	}

	public void listFormateursTest() {
		List list = formateurService.listFormateurs();
		assertTrue(list.size() > 0);
	}

	public void noterFormateurTest() throws NotFoundException {
		Formateur form = formateurRepository.getFormateurByName(formateur.getNom());
		double oldNote = form.getNote();
		double newnote = formateurService.noterFormateur(15, 5);
		assertNotEquals(oldNote, newnote);
	}

}
