package com.esprit.examen;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.esprit.examen.entities.Cours;
import com.esprit.examen.entities.TypeCours;
import com.esprit.examen.repositories.CoursRepository;
import com.esprit.examen.services.CoursService;
@SpringBootTest
@RunWith(SpringRunner.class)
public class CoursServiceTest {
	
	@Autowired
	CoursRepository coursRepository ;
	@Autowired
	CoursService coursService;
	private Cours cours;

	@DisplayName("Test AjouterCours")

	@Before
	public void setUp()
	{
		this.cours = new Cours();
		this.cours.setDescription("Scrum Master");
		this.cours.setIntitule("scrum methodology");
		this.cours.setTypeCours(TypeCours.SCRUM);

	}
	
	
	@Test
	public void testAjouterCours (){
	Long idCours = coursService.addCours(cours);
	assertEquals(cours.getId(), idCours);
	coursService.supprimerCours(idCours);

	}
	@DisplayName("countDataBeforeAfterTest")
	public void countDataBeforeAfterTest() {
		Long dataBeforeTest=null;
		try {
			dataBeforeTest =coursRepository.count();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Long idCours=coursService.addCours(cours);
//		coursRepository.save(cours);
		Long dataAfterTest = null;
		try {
			dataAfterTest = coursRepository.count();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		assertThat(dataBeforeTest).isEqualTo(dataAfterTest -1);
		assertEquals(dataBeforeTest, dataAfterTest -1);
		coursRepository.delete(cours);
	}
	
	@DisplayName("testSupprimerCoursById")
	public void testSupprimerCoursById() {
		coursService.supprimerCours(cours.getId());
		assertThat(cours.getId()).isNotNegative();
	}

	@DisplayName("testListCours")
	public void testListCours() {
		List list = coursService.getCours();
		assertTrue(list.size() > 0);
	}
	
	@DisplayName("testModifierCours")
	public void testModifierCours() {
	
	}
	
}
