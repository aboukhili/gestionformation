FROM openjdk:8-jdk-alpine
EXPOSE 8083
ADD target/GesF-1.0.0-SNAPSHOT.jar gestionF-devops-1.0.jar
ENTRYPOINT ["java","-jar","/gestionF-devops-1.0.jar"]

